function countLetter(letter, sentence) {
    let count = 0;
    for (let i = 0; i < sentence.length; i++) {
         if (sentence.charAt(i) == letter) {
            count += 1;
        }
    }
    return(letter.length===1?
        count
        :
        undefined
    )
    
    // return count;
    // Check first whether the letter is a single character.
    // If letter is a single character, count how many times a letter has occurred in a given sentence then return count.
    // If letter is invalid, return undefined.
    ////return (sentence.split(letter)).length - 1;
    //yey okay na
    
}
countLetter("l","camille");
countLetter("camille","camille");

function isIsogram(text) {

   text = text.toLowerCase().split('')
   const output = text.reduce((word,letter)=>{
    !word.includes(letter)&&word.push(letter);
    return word;
   },[]);
   return output.length === text.length;
    //  x = false; y = false;
    // for(i = 0; i < text.length; i++){
    //     wordl = text.substring(0,i)
    //     wordr = text.substring(i)
    //     x = wordl.includes(text.charAt(i))
    //     y = wordr.includes(text.charAt(i))
    //     //console.log(x,wordl,wordr)
    // }
    // return x&&y


    // An isogram is a word where there are no repeating letters.
    // The function should disregard text casing before doing anything else.
    // If the function finds a repeating letter, return false. Otherwise, return true.
    //done

    
}

isIsogram("donel");//False
isIsogram("camille"); //True


function purchase(age, price) {
    // Return undefined for people aged below 13.
    // Return the discounted price (rounded off) for students aged 13 to 21 and senior citizens. (20% discount)
    // Return the rounded off price for people aged 22 to 64.
    // The returned value should be a string.
    //Math.round

    if(age<13){
        
        return undefined
    }
    else if ((age>=13 && age<=21) || age>=65) {
        return String(Math.round((price*.80)*100)/100);
    }
    else {
        return String(Math.round(price*100)/100);
    }



}

function findHotCategories(items) {

     hotCateg = []

    function checkStocks(item){
        if(item.stocks === 0){
            return item
        }
    }
    
    hotItems = items.filter(checkStocks)
    
   
    for(x in hotItems){
        if (hotCateg.indexOf(hotItems[x].category) === -1){ 
            hotCateg.push(hotItems[x].category)
        }
    }

    return hotCateg; 

    // let index = items.find(item=>item.stocks===0);
    // return(items.category)
    // Find categories that has no more stocks.
    // The hot categories must be unique; no repeating categories.

    // The passed items array from the test are the following:
    // { id: 'tltry001', name: 'soap', stocks: 14, category: 'toiletries' }
    // { id: 'tltry002', name: 'shampoo', stocks: 8, category: 'toiletries' }
    // { id: 'tltry003', name: 'tissues', stocks: 0, category: 'toiletries' }
    // { id: 'gdgt001', name: 'phone', stocks: 0, category: 'gadgets' }
    // { id: 'gdgt002', name: 'monitor', stocks: 0, category: 'gadgets' }

    // The expected output after processing the items array is ['toiletries', 'gadgets'].
    // Only putting return ['toiletries', 'gadgets'] will not be counted as a passing test during manual checking of codes.

}

const items = [
{ id: 'tltry001', name: 'soap', stocks: 14, category: 'toiletries' },
{ id: 'tltry002', name: 'shampoo', stocks: 8, category: 'toiletries' },
{ id: 'tltry003', name: 'tissues', stocks: 0, category: 'toiletries' },
{ id: 'gdgt001', name: 'phone', stocks: 0, category: 'gadgets' },
{ id: 'gdgt002', name: 'monitor', stocks: 0, category: 'gadgets' }
]

findHotCategories(items);









function findFlyingVoters(candidateA, candidateB) {

    
return candidateA.filter(obj=> candidateB.indexOf(obj) !== -1);

    // candidateA.filter(element => candidateB.includes(element))


    // Find voters who voted for both candidate A and candidate B.

    // The passed values from the test are the following:
    // candidateA: ['LIWf1l', 'V2hjZH', 'rDmZns', 'PvaRBI', 'i7Xw6C', 'NPhm2m']
    // candidateB: ['kcUtuu', 'LLeUTl', 'r04Zsl', '84EqYo', 'V2hjZH', 'LIWf1l']

    // The expected output after processing the candidates array is ['LIWf1l', 'V2hjZH'].
    // Only putting return ['LIWf1l', 'V2hjZH'] will not be counted as a passing test during manual checking of codes.
    /////done
    
}
const candidateA = ['LIWf1l', 'V2hjZH', 'rDmZns', 'PvaRBI', 'i7Xw6C', 'NPhm2m'];
const candidateB = ['kcUtuu', 'LLeUTl', 'r04Zsl', '84EqYo', 'V2hjZH', 'LIWf1l']

findFlyingVoters(candidateA,candidateB);

module.exports = {
    countLetter,
    isIsogram,
    purchase,
    findHotCategories,
    findFlyingVoters
};